## TelegramGPT
simple basic python script to introduce Telegram Ai conversation with DALL-E integration

Demo example (web)

![Unknown](https://user-images.githubusercontent.com/101345730/229813717-3d772515-f702-4e65-a830-a88f5639fe5f.jpeg)


## Configuration 
You need to have a personal [Telegram BOT](https://core.telegram.org/bots) or create it using [Botfather](https://t.me/botfather) 

when you are connected to your OpenAi account got your profile settings -> API
- [ /!\ Do not share your API key, be careful ! ]

![Pasted-e1663692850364](https://user-images.githubusercontent.com/101345730/229800885-4f6f9af2-652f-46e5-8dbc-6364309bce55.png)

Some example here ! 

![image-1657264719754-compressed png](https://user-images.githubusercontent.com/101345730/229801233-de67cae5-4c81-4695-b440-e7e400e18e81.jpeg)


## How to get your chat ID ? 

you need to use this bot to retrieve your ID 

- [https://t.me/@userinfobot](https://telegram.im/@userinfobot)
- 
- ![chatID](https://user-images.githubusercontent.com/101345730/229802467-67b0271c-df52-4c73-a421-a20a4d8a15f9.png)

## Troubleshooting
if you get the following error message when you run the script:
`openai.error.InvalidRequestError: Billing hard limit has been reached` it means your free trial period is over and that you need to set up payment to generate images. <br>
Go to the following links for more info: <br>
- [General OpenAI pricing](https://openai.com/api/pricing/) ( You'd more be concerned with the Image Model pricing)
- [Set up Billing information and usage limits](https://beta.openai.com/account/billing/overview)

If you do not have ability to pay for images (and you have already used up your free credts or they are expired), I unfortunately can't help you since the Image model is on a pay per use basis.

# Requirements (python modules)
 
 Python3
  
- [Openai 0.27.3](https://pypi.org/project/openai/)
- [requests 2.28.2](https://pypi.org/project/requests/)
- [telegram 0.0.1](https://pypi.org/search/?q=telegram)
