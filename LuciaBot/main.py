import openai
import requests
import telegram
from telegram.ext import  Updater, CommandHandler, MessageHandler, Filters

# Define your OpenAI API key
openai.api_key = "Your custom openAi API key"

# Get the Telegram bot token and chat ID
bot_token = "your bot token" #BOT_TOKEN
chat_id = "your chat ID" #chat ID telegram

# Define the function to send a message to the Telegram bot
def send_telegram_message(message):
    bot = telegram.Bot(token=bot_token)
    bot.send_message(chat_id=chat_id, text=message)

# Define the function to handle incoming messages
def handle_message(update, context):
    message_text = update.message.text
    
    # Generate an image using DALL-E
    image_response = openai.Image.create(
        model="image-alpha-001",
        prompt=message_text,
        size="1024x1024"
    )

    image_url = image_response['data'][0]['url']
    file_url = requests.get(image_url, stream=True)
    send_photo_url = f"https://api.telegram.org/bot{bot_token}/sendPhoto"
    response = requests.post(send_photo_url, data={"chat_id": chat_id}, files={"photo": file_url.raw})
    
    # Generate text using GPT-3
    gpt_response = openai.Completion.create(
        engine="davinci",
        prompt=message_text,
        max_tokens=500,
        n=1,
        stop=None,
        temperature=0.6,
    )
    gpt_text = gpt_response.choices[0].text.strip()
    message = f"Image generated for prompt: {message_text}\nGPT-3 response: {gpt_text}"
    send_telegram_message(message)

# Set up the Telegram bot with the appropriate handlers
bot = telegram.Bot(token=bot_token)
updater = Updater(token=bot_token)
dispatcher = updater.dispatcher
message_handler = MessageHandler(Filters.text, handle_message)
dispatcher.add_handler(message_handler)
updater.start_polling()
